package com.beacon.link;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

public class SingleLinkDemo {

    /**
     * 使用水浒传的例子来驱动 linkList 逻辑的实现
     * 特点：
     * 1、head元素仅作为标记使用，其中仅next字段值不为空，指向链表中的第一个元素
     * 2、每个元素通过 next 字段作为关联，以此来实现链表的功能
     * 3、
     * */
    public static void main(String[] args) {

        SingleLinkList singleLinkList = new SingleLinkList();
        //添加元素
        singleLinkList.add(new HeroNode(4, "宋江", "及时雨"));
        singleLinkList.add(new HeroNode(2, "卢俊义", "玉麒麟"));
        singleLinkList.add(new HeroNode(3, "林冲", "豹子头"));
        singleLinkList.add(new HeroNode(1, "吴用", "智多星"));
        //打印链表中的全部元素
        singleLinkList.showAllElement();
        singleLinkList.showAllElementCount();
    }


    /**
     * 特点：
     * 1、head字段仅作为标记使用，仅next字段值不为空，指向链表中的第一个元素
     * 2、链表中每个元素通过 next 字段关联，以此来实现链表的功能
     * */
    private static class SingleLinkList{

        //head元素仅作为标记使用，在使用过程中其中，仅next字段值不为空，用于指向链表中的第一个元素
        private final HeroNode head = new HeroNode(0, "", "");


        /**
         * 向队列添加元素
         * 逻辑：找到当前链表的最后一个元素，并将该元素的next的字段值指向当前新增元素
         * */
        public void add(HeroNode heroNode) {
            //如果为空直接将当前元素与链表的head建立关系
            if (this.isEmpty()) {
                head.setNext(heroNode);
            } else {
                //如果不为空，找到链表的最后一个元素 (若元素的next为null则表示为最后一个元素)
                HeroNode temp = head.next;
                while (temp.next != null) {
                    temp = temp.next;
                }

                //并将其next字段值指向当前新增元素
                temp.next = heroNode;
            }
        }

        /**
         * 判断链表是否为空
         * */
        public boolean isEmpty() {
            return head.next == null;
        }


        /**
         * 展示链表中的全部元素
         * */
        public void showAllElement() {
            if (this.isEmpty()) {
                System.out.println("链表为空");
                return;
            }
            System.out.println("链表中的元素如下：");
            HeroNode temp = head.next;
            while (temp != null) {
                System.out.println(temp.toString());
                temp = temp.next;
            }
        }

        /**
         * 获取全部元素的数量
         * */
        public void showAllElementCount() {
            int flag = 0;

            if (this.isEmpty()) {
                System.out.println(flag);
                return;
            }

            HeroNode temp = head.next;
            while (true) {
                if (temp != null) {
                    flag++;
                    temp = temp.getNext();
                } else {
                    break;
                }
            }
            System.out.println(flag);
        }

        /**
         * 查找链表中的倒数第n个元素
         * 1、单向链表只有一个head，如果需要获取倒数的数据，需要用逆向思维的方式去用全部有效元素的个数减去倒数坐标数，将获取获取第几个的问题转换为正数第几个的问题。
         * 2、要考虑倒数的index超出实际情况的场景。
         */
        public void findByIndex() {

        }

        /**
         * 反转链表
         * 1、搞一个新的reverseHead 遍历原链表，每遍历一个元素就将其保存在reverseHead的后面，实现 head insert 头插
         * 2、将原来的head的next指向reverseHead的next即可
         */
        public void reverseList() {

        }


    }


    /**
     * 链表节点
     * 1、next字段是实现链表关键字段，通过next字段将不同元素建立关系
     * 2、no、name、nickName 字段用于存储元素信息
     * */
    @Setter
    @Getter
    private static class HeroNode{
        private int no;
        private String name;
        private String nickName;
        private HeroNode next;

        public HeroNode(int no, String name, String nickName) {
            this.no = no;
            this.name = name;
            this.nickName = nickName;
        }

        @Override
        public String toString() {
            return "HeroNode{" +
                    "no=" + no +
                    ", name='" + name + '\'' +
                    ", nickName='" + nickName + '\'' +
                    '}';
        }
    }
}
