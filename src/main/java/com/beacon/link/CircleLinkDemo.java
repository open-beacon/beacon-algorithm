package com.beacon.link;

/**
 * 单向环形表链
 */
public class CircleLinkDemo {

    /**
     * 约瑟夫问题通常使用单向环形链表的数据结构来处理
     * 问题详情：设编号为1，2，3 ... n，n个人围成一圈，约定编号为k（1<=k<=n）的人从1开始报数，数到m的那个人出列，他的下一位又从1开始报数，数到m的人出列，以此类推，直到所有人出列为止，由此产生一个出队编号的序列。
     * 1、单向链表在修改的时候依赖于前置节点，这是一个关键点
     * 2、实现约瑟夫的问题关键点在于 递归
     * */
    public static void main(String[] args) {
        CircleLinkList circleLinkList = new CircleLinkList();
//        circleLinkList.init(10);
//        circleLinkList.showAll();

        //出队列顺序：24153
        circleLinkList.josephusProblem(5, 1, 2);
    }

    /**
     * 环形队列实现思路：
     * 1、定义一个first字段用于指向环形队列的首部
     * 2、创建新元素
     * 2、如果创建的元素是环形链表中的第一个节点需要做特殊处理，需要将first指向环形队列中的第一个元素
     * 3、将当前尾部元素的next设置为当前元素，将新元素的next指向first，将新增元素设置为环形队列的尾部
     * */
    private static class CircleLinkList {

        /**
         * 环形链表的头部
         * */
        private LinkNode<String> first = null;

        /**
         * 初始化环形队列
         * 添加元素的数量
         * */
        public void init(int count) {
            LinkNode<String> tail = null;
            for (int i = 0; i < count; i++) {
                LinkNode<String> newLinkNode = new LinkNode<>(String.valueOf(i + 1));
                //判断当前是否是第一个元素
                if (i == 0) {
                    first = newLinkNode;
                    first.setNext(first);
                    tail = newLinkNode;
                } else {
                    tail.setNext(newLinkNode);
                    newLinkNode.setNext(first);
                    tail = newLinkNode;
                }
            }
        }

        /**
         * 按照从头到尾的顺序展示环形队列中的全部元素
         * */
        public void showAll() {

            if (this.isEmpty()) {
                System.out.println("环形队列为空~");
                return;
            }

            System.out.println("以下信息为环形队列中的元素：");
            LinkNode<String> current = first;
            int i = 0;
            while (true) {
                System.out.println("当前是环形队列中的第" + i + "个元素,内容为" + current.getData());
                if (current.getNext() == first) {
                    System.out.println("到达环形队列尾部，结束循环。");
                    break;
                }
                current = current.getNext();
                i++;
            }
        }

        private boolean isEmpty() {
            return first == null;
        }

        /**
         * 约瑟夫问题
         */
        public void josephusProblem(int n, int k, int m) {
            if (k > n) {
                System.out.println("k 大于 n 结束执行");
                return;
            }
            //初始化一个环形队列
            CircleLinkList circleLinkList = new CircleLinkList();
            circleLinkList.init(n);

            //找到第k个元素的前一个节点
            LinkNode<String> preStartNode = circleLinkList.getTail();
            while (k > 1) {
                preStartNode = preStartNode.getNext();
                k--;
            }

            this.output(preStartNode, m);

        }

        public void output(LinkNode<String> preStartNode, int m) {
            if (preStartNode == preStartNode.getNext()) {
                //最后一个元素
                System.out.println("最后一个元素出列：" + preStartNode.getData());
                return;
            }
            int m1 = m;
            while (true) {
                m1--;
                if (m1 == 0) {
                    String data = preStartNode.getNext().getData();
                    System.out.println("出队列:" + data);
                    preStartNode.setNext(preStartNode.getNext().getNext());
                    this.output(preStartNode, m);
                    System.out.println("递归结束了：" + data);
                    break;
                } else {
                    preStartNode = preStartNode.getNext();
                }
            }
        }

        public LinkNode<String> getTail() {
            LinkNode<String> tail = first;
            while (true) {
                if (tail.getNext() == first) {
                    return tail;
                } else {
                    tail = tail.getNext();
                }
            }
        }
        public LinkNode<String> getFirst() {
            return this.first;
        }

    }
}
