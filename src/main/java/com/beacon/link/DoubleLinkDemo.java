package com.beacon.link;

/**
 * 双向链表
 */
public class DoubleLinkDemo {

    /**
     * 双向链表与单向链表的区别
     * 1、可以从两个方向进行遍历使用
     * 2、删除节点不再依赖于前一个节点
     *
     * 双向链表核心实现特点
     * 1、每个节点都有两个引用，一个指向下一个节点，一个指向前一个节点
     * */
    public static void main(String[] args) {

    }
}
