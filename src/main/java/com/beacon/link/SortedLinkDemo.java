package com.beacon.link;

import lombok.Getter;
import lombok.Setter;

public class SortedLinkDemo {

    /**
     * 使用水浒传的例子来驱动逻辑的实现
     * 特点：
     * 1、head元素仅作为标记使用，其中仅next字段值不为空，指向链表中的第一个元素
     * 2、每个元素通过 next 字段作为关联，以此来实现链表的功能
     * 3、每个元素按照从小到大的顺序来进行，那么加入的位置就不是最后了，需要从第一个元素开始挨个比较，直到找到一个大于当前，小于当前的next的元素，将其放在二者中间即可
     * */
    public static void main(String[] args) {

        SingleLinkList singleLinkList = new SingleLinkList();
        //添加元素
        singleLinkList.add(new HeroNode(1, "宋江", "及时雨"));
        singleLinkList.add(new HeroNode(8, "宋江", "及时雨"));
        singleLinkList.add(new HeroNode(10, "卢俊义", "玉麒麟"));
        singleLinkList.add(new HeroNode(30, "卢俊义", "玉麒麟"));
        singleLinkList.add(new HeroNode(3, "林冲", "豹子头"));
        singleLinkList.add(new HeroNode(4, "林冲", "豹子头"));
        singleLinkList.add(new HeroNode(5, "吴用", "智多星"));
        singleLinkList.add(new HeroNode(2, "吴用", "智多星"));
        //打印链表中的全部元素
        singleLinkList.showAllElement();

        singleLinkList.update(new HeroNode(30, "吴用1", "智多星1"));


        System.out.println("删除no:" + 11);
        singleLinkList.delete(11);
        singleLinkList.showAllElement();

    }


    /**
     * 特点：
     * 1、head字段仅作为标记使用，仅next字段值不为空，指向链表中的第一个元素
     * 2、链表中每个元素通过 next 字段关联，以此来实现链表的功能
     * */
    private static class SingleLinkList{

        //head元素仅作为标记使用，在使用过程中其中，仅next字段值不为空，用于指向链表中的第一个元素
        private final HeroNode head = new HeroNode(0, "", "");


        /**
         * 向队列添加元素
         * 逻辑：找到当前链表的最后一个元素，并将该元素的next的字段值指向当前新增元素
         * */
        public void add(HeroNode heroNode) {
            //如果为空直接将当前元素与链表的head建立关系
            if (this.isEmpty()) {
                head.setNext(heroNode);
            } else {
                //按照排名从小到大进行排序，那么当加入一个元素的时候，新加入的元素应该放在第一个大于它的元素前面
                checkAndAdd(heroNode, head);
            }
        }

        /**
         * 有了当前值，就有了当前及后一个节点的值
         *      判断1：如果当前元素的下一个元素如果null，直接将新元素作为当前元素的下一个元素
         *      判断2：如果新加入的元素小于当前元素下一个元素的值，那么就找到了位置，将当前元素的next指向新加入的元素，新加入的元素指向原
         *      判断3：如果新加入的元素等于当前值，链表中已存在，无需向其中添加元素
         *      递归逻辑：如果新加入的元素大于当前值，那么直接进入递归调用，继续执行上述两个判断逻辑，直至找到最终归属
         * */
        private void checkAndAdd(HeroNode heroNode, HeroNode currentElement) {

            //判断1
            if (null == currentElement.next) {
                currentElement.next = heroNode;
                //找到了合适的位置无需执行下面的逻辑，直接return即可
                return;
            }

            HeroNode nextElement = currentElement.next;

            if (heroNode.getNo() < nextElement.getNo()) {
                //判断2
                currentElement.next = heroNode;
                heroNode.next = nextElement;
            } else if (heroNode.getNo() == nextElement.getNo()) {
                //判断3
                System.out.println("当前位置已经有人了");
            }else {
                //递归逻辑
                this.checkAndAdd(heroNode, nextElement);
            }

        }



        /**
         * 根据no删除元素
         * 思路:
         * 1、根据no找到对应的元素，元素根据no从小到大排序
         *    从head开始遍历如果直至找到了一个元素的no大于当前需要修改元素的no还是没找到，那么表示该元素在链表中不存在。
         * 2、步骤1找到的temp的next就是no所对应的元素，将temp的next指向temp的next的next。
         * */
        public void delete(int no) {
            //排除为空的情况
            if (this.isEmpty()) {
                System.out.println("linked list is empty");
                return;
            }
            //步骤一：根据no找到对应的元素
            HeroNode temp = head;
            while (true) {
                HeroNode tempNext = temp.next;
                if (tempNext == null) {
                    System.out.println("未找到满足条件的no");
                    showAllElement();
                    return;
                }
                if (no > tempNext.getNo()) {
                    temp = temp.next;
                } else if (no == tempNext.getNo()) {
                    break;
                } else {
                    System.out.println("未找到满足条件的no");
                    showAllElement();
                    return;
                }
            }

            //步骤二：temp的next为当前no对应的元素，也就是我们需要删除的值，我们只需将temp的next指向temp的next的next即可实现删除的效果
            temp.next = temp.getNext().getNext();
        }



        /**
         * 根据新加入元素的no来修改
         * 思路:
         * 1、根据no找到对应的元素，元素根据no从小到大排序
         *    从head开始遍历如果直至找到了一个元素的no大于当前需要修改元素的no还是没找到，那么表示该元素在链表中不存在。
         * 2、找到了对应的元素后，仅修改name、nickName即可
         * */
        public void update(HeroNode newHeroNode) {
            //排除为空的情况
            if (this.isEmpty()) {
                System.out.println("linked list is empty");
                return;
            }
            //步骤一：根据no找到对应的元素
            int no = newHeroNode.getNo();

            HeroNode temp = head.next;
            while (true) {
                if (temp == null) {
                    System.out.println("未找到满足条件的no");
                    showAllElement();
                    return;
                }
                if (no > temp.getNo()) {
                    temp = temp.next;
                } else if (no == temp.getNo()) {
                    break;
                } else {
                    System.out.println("未找到满足条件的no");
                    showAllElement();
                    return;
                }
            }

            //步骤二：
            //此时的temp已被赋值为我们所需修改的元素，直接进行修改即可
            System.out.println("修改前");
            showAllElement();
            temp.setName(newHeroNode.getName());
            temp.setNickName(newHeroNode.getNickName());
            System.out.println("修改后");
            showAllElement();
        }

        /**
         * 判断链表是否为空
         * */
        public boolean isEmpty() {
            return head.next == null;
        }


        /**
         * 展示链表中的全部元素
         * */
        public void showAllElement() {
            if (this.isEmpty()) {
                System.out.println("链表为空");
                return;
            }
            System.out.println("链表中的元素如下：");
            HeroNode temp = head.next;
            while (temp != null) {
                System.out.println(temp.toString());
                temp = temp.next;
            }
        }

    }

    /**
     * 链表节点
     * 1、next字段是实现链表关键字段，通过next字段将不同元素建立关系
     * 2、no、name、nickName 字段用于存储元素信息
     * */
    @Setter
    @Getter
    private static class HeroNode{
        private int no;
        private String name;
        private String nickName;
        private HeroNode next;

        public HeroNode(int no, String name, String nickName) {
            this.no = no;
            this.name = name;
            this.nickName = nickName;
        }

        @Override
        public String toString() {
            return "HeroNode{" +
                    "no=" + no +
                    ", name='" + name + '\'' +
                    ", nickName='" + nickName + '\'' +
                    '}';
        }
    }
}
