package com.beacon.link;

import lombok.Data;

/**
 * 链表节点
 */
@Data
public class LinkNode<T> {

    /**
     * 前节点
     */
    private LinkNode<T> previous;
    /**
     * 当前节点值
     */
    private T data;
    /**
     * 后一个节点
     */
    private LinkNode<T> next;

    public LinkNode(T data) {
        this.data = data;
    }
}
