package com.beacon.queue;

/**
 * 队列特性是先进先出，使用数组模拟队列的特性
 */
public class QueueArrayDemo {


    /**
     * 实现思路：
     *  环形数队列，通过对数组下标，取模来实现。
     *
     *  特点：
     *  1、front 指向数组中第一个元素，初始值为0
     *  2、rear 指向数组中最后一个元素的下一个位置，rear的初始值为0
     *  3、队列满的条件为：（rear+1）%maxSize = front
     *  4、队列为空的条件为：rear=front
     *  5、队列中有效值的个数计算方式：(rear+maxSize-front)%maxSize
     *  6、数组长度比队列长度大1，如果不大1的话，按照现在的实现思路无法区分队列满还是空，空一个元素避免了头和尾亲上。
     * */
    public static void main(String[] args) {
        //构造一个长度为4的队列
        QueueArray queue = new QueueArray(4);
        queue.add(1);
        System.out.printf("从队列中取出一个元素 %d\n",queue.get());
        queue.add(2);
        System.out.printf("从队列中取出一个元素 %d\n",queue.get());
        queue.add(1);
        queue.add(1);
        queue.add(1);
        queue.add(1);
        queue.add(1);

        //展示当前数组的情况
        System.out.println("当前队列情况如下：");
        queue.showQueueAllElement();

        //查看头部元素
        System.out.println("当前队列的头部元素为：" + queue.showQueueHeadElement());
    }

    private static class QueueArray {
        private final int maxSize;
        private final int[] array;
        /**指向队列头的前一个位置，默认值为 -1*/
        private int front;
        /**指向队列尾的位置，默认值为 -1*/
        private int rear;

        /**构造函数*/
        public QueueArray(int maxSize) {
            this.maxSize = maxSize + 1;
            this.front = 0;
            this.rear = 0;
            this.array = new int[this.maxSize];
        }

        /**判断队列是否满*/
        public boolean isFull() {
            return (rear + 1) % maxSize == front;
        }

        /**
         * 判断队列是否为空
         */
        public boolean isEmpty() {
            return front == rear;
        }

        /**
         * 向队列添加一个元素
         * @param element 元素
         */
        public void add(int element) {
            //判断队列是否满？
            if (isFull()) {
                System.out.println("向队列添加元素失败，队列已满");
                return;
            }

            //rear指向的位置就是队列尾部的后一位，所以直接赋值即可
            array[rear] = element;
            //增加完元素后，需要将rear向后调整一位，注意这里因为是循环数组的原因，需要对数组下标取模
            rear = (rear + 1) % maxSize;
            System.out.println("成功向队列添加一个元素，值为：" + element);
        }


        /**
         * 从队列中取出一个元素
         */
        public int get() {
            //判断队列是否为空
            if (isEmpty()) {
                System.out.println("队列为空，无数据");
                throw new RuntimeException("queue is empty");
            }

            int element = array[front];
            //考虑是循环队列，所以需要将front向后调整一位并对队列长度取模
            front = (front + 1) % maxSize;
            return element;
        }

        /**展示全部队列中的值*/
        public void showQueueAllElement() {
            if (isEmpty()) {
                System.out.println("队列为空");
                return;
            }

            //遍历存在两种情况，尾部在头部前和头部后两种
            if (front < rear) {
                for (int i = front; i < rear; i++) {
                    System.out.printf("元素在数组中的位置为%d，值为%d \n", i, array[i]);
                }
            } else {
                //此逻辑实际上是可以处理 front > rear 和 < rear 两种情况，分开的原因是上面的for循环效率更高。
                for (int i = front; i != rear; i = (i + 1) % maxSize) {
                    System.out.printf("元素在数组中的位置为%d，值为%d \n", i, array[i]);
                }
            }

        }

        /**
         * 查看队列中的头部元素
         * @return 队列中的头部元素
         * */
        public int showQueueHeadElement() {
            if (isEmpty()) {
                System.out.println("队列为空");
                throw new RuntimeException("queue is empty");
            }
            return array[front];
        }
    }
}
