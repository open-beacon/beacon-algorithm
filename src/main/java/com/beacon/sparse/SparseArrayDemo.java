package com.beacon.sparse;

import java.util.Arrays;

/**
 * 稀疏数组
 * */
public class SparseArrayDemo {



    /**
     * 二维数组转稀疏数组的过程就是压缩的过程
     * 稀疏数组转二维数组的过程就是解压的过程
     *
     * 实现思路：
     * 1、获取原始二维数组中的有效值个数，以及原始二维数组的规模（该信息用于解压）
     * 2、根据有效值的个数创建稀疏数组，初始化稀疏数组的第一行数据（第一行数据记录二维数组的行数、列数、有效值数）
     * 3、再次遍历二维数据，将二维数组中的有效值以及有效值的坐标保存至稀疏数组中
     *
     * 特性：
     * 1、压缩二维中的默认值数据得到占用空间更小的二维数组
     * 2、增加默认值情况比较多的思路
     * */
    public static void main(String[] args) {
        //初始化随机创建一个二维数组
        int[][] initDoubleArray  = new int[10][10];
        initDoubleArray [1][2]=1;
        initDoubleArray [1][3]=2;
        initDoubleArray [3][2]=1;
        initDoubleArray [3][3]=2;

        //二维数据转稀疏数组
        int[][] sparseArray = doubleArrayTransferToSparseArray(initDoubleArray);

        //稀疏数组转二维数组
        int[][] doubleArray = sparseArrayTransferToDoubleArray(sparseArray);

    }

    /**
     * 稀疏数组转二维数组
     * @param sparseArray 稀疏数组
     * @return 二维数组
     * */
    private static int[][] sparseArrayTransferToDoubleArray(int[][] sparseArray) {
        //步骤一：根据稀疏数组的首行信息构造一个二维数组
        int[] firstLine = sparseArray[0];
        int[][] doubleDimensionalArray = new int[firstLine[0]][firstLine[1]];

        //步骤二：二维数组赋值
        // 从稀疏数组的第二行开始，记录着二维数组的坐标位置及坐标值，将其值赋值给二维数组
        for (int i = 1; i < sparseArray.length; i++) {
            int[] line = sparseArray[i];
            doubleDimensionalArray[line[0]][line[1]] = line[2];
        }
        System.out.println("\n 原始的稀疏数组如下：");
        System.out.println(Arrays.deepToString(sparseArray));
        System.out.println("转换后的二维数组如下：");
        System.out.println(Arrays.deepToString(doubleDimensionalArray));
        return doubleDimensionalArray;
    }


    /**
     * 二维数组转稀疏数组
     * @param doubleDimensionalArray 二维数组
     * @return 稀疏数组
     * */
    private static int[][] doubleArrayTransferToSparseArray(int[][] doubleDimensionalArray) {
        //获取数组中有效值的个数
        int efficientValueCount = 0;
        for (int[] array : doubleDimensionalArray) {
            for (int element : array) {
                if (0 != element) {
                    efficientValueCount++;
                }
            }
        }

        //步骤一：初始化一个稀疏数组
        // 根据有效值的个数及二维数组的行数、列数来创建稀疏数据，完成稀疏数组的初始化
        int[][] sparseArray = new int[efficientValueCount + 1][3];

        //步骤二：初始化第一行数据，即二维数据的行数、列数、有效值个数
        int[] line0 = sparseArray[0];
        line0[0] = doubleDimensionalArray.length;
        line0[1] = doubleDimensionalArray[0].length;
        line0[2] = efficientValueCount;

        // 步骤三：向稀疏数组中记记录二维数组中每个值的详细坐标及有效值
        int efficientValueIndex = 1;
        for (int i = 0; i < doubleDimensionalArray.length; i++) {
            for (int j = 0; j < doubleDimensionalArray[i].length; j++) {
                int elementValue = doubleDimensionalArray[i][j];
                if (0 != elementValue) {
                    int[] line = sparseArray[efficientValueIndex];
                    line[0] = i;
                    line[1] = j;
                    line[2] = elementValue;
                    System.out.println("命中一个有效值，详情如下：i:"+i+" j:"+j+" elementValue:"+elementValue);
                    efficientValueIndex++;
                }
            }
        }

        //打印二维数组和稀疏数组
        System.out.println("\n 原始的二维数组如下：");
        System.out.println(Arrays.deepToString(doubleDimensionalArray));
        System.out.println("稀疏数组如下：");
        System.out.println(Arrays.deepToString(sparseArray));
        return sparseArray;
    }

}
