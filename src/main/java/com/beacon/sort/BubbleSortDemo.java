package com.beacon.sort;


import java.util.Arrays;

/**
 * 冒泡排序
 * 描述：冒泡排序（Bubble Sort）也是一种简单直观的排序算法。它重复地走访过要排序的数列，一次比较两个元素，如果他们的顺序错误就把他们交换过来。
 * 走访数列的工作是重复地进行直到没有再需要交换，也就是说该数列已经排序完成。这个算法的名字由来是因为越小的元素会经由交换慢慢"浮"到数列的顶端
 * 时间复杂度：线性阶 O(n)
 * @author Beacon
 */
public class BubbleSortDemo {


    public static void main(String[] args) {

        int[] a1 = {1, 2, 3, 4, 5};
        System.out.println(Arrays.toString(a1));

        for (int i = 0; i < a1.length-1; i++) {
            //结束循环标志
            boolean breakLoopFlag = true;

            for (int j = 0; j < a1.length - 1 - i; j++) {
                int before = a1[j];
                int after = a1[j + 1];
                //如果前面的大于后面的则调换位置
                if (before > after) {
                    a1[j] = after;
                    a1[j + 1] = before;
                    breakLoopFlag = false;
                }
            }

            //如果存在某一轮处理过程中未调换过任何一次位置则数组顺序已排序完成，则直接结束循环
            if (breakLoopFlag) {
                break;
            }
        }

        System.out.println("排序后的效果");
        System.out.println(Arrays.toString(a1));
    }
}
