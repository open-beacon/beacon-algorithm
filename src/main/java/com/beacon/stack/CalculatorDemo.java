package com.beacon.stack;

/**
 * 根据栈来实现计算器
 * 使用栈完成表达式的计算思路
 * 1.通过一个index 值(索引)，来遍历我们的表达式
 * 2.如果我们发现是一个数字,就直接入数栈
 * 3.如果发现扫描到是一个符号,就分如下情况
 *   3.1如果发现当前的符号栈为空，就直接入栈
 *   3.2如果符号栈有操作符，就进行比较
 *      3.2.1如果当前的操作符的优先级小于或者等于栈中的操作符，就需要从数栈中pop出两个数,在从符号栈中pop出一个符号，进行运算，将得到结果，入数栈，然后将当前的操作符入符号栈。
 *      3.2.2如果当前的操作符的优先级大于栈中的操作符，就直接入符号栈.
 * 4.当表达式扫描完毕，就顺序的从数栈和符号栈中pop出相应的数和符号进行运算
 * 5.最后在数栈只有一个数字，就是表达式的结果
 */
public class CalculatorDemo {

    public static void main(String[] args) {
        //String expression = "3+2*6-2+10/2";
        String expression = "3+2*6-2+20/10+100";
        //创建两个栈，一个数栈一个操作栈
        StackDemo.ArrayStack numberStack = new StackDemo.ArrayStack(10);
        StackDemo.ArrayStack operatorStack = new StackDemo.ArrayStack(10);

        int index = 0;
        int number1 = 0;
        int number2 = 0;
        int operator = 0;
        int result = 0;
        //每次循环到的字符保存至该字段中
        char currentChar = ' ';
        String multiDigitNumber = "";

        while (true) {
            //while循环 得到表达式中的每个字符
            currentChar = expression.substring(index, index + 1).charAt(0);

            //判断当前运算符是否是操作符
            if (isOperator(currentChar)) {
                //判断当前符号栈是否为空，为空则直接压入
                if (operatorStack.isEmpty()) {
                    operatorStack.push(currentChar);
                } else {
                    //如果符号栈不为空，则进行比较，比较结果存在以下两种情况。
                    //情况一：如果当前操作符的优先级小于或者等于操作符栈顶的操作符，就需要从数栈中弹出两个数字，从操作符栈中弹出一个操作符，进行计算并将结果压入数字栈中。
                    //情况二：如果当前操作符的优先级大于操作符栈顶的操作符,则将操作符入栈即可
                    int topOperator = operatorStack.peak();
                    if (checkPriority(currentChar) <= checkPriority(topOperator)) {
                        //情况一
                        number2 = numberStack.pop();
                        number1 = numberStack.pop();
                        operator = operatorStack.pop();
                        numberStack.push(calculate(number1, number2, operator));
                        operatorStack.push(currentChar);
                    } else {
                        //情况二
                        operatorStack.push(currentChar);
                    }
                }
            } else {

                //当前符号为数字直接入栈，这里为什么是48可以参考ASCII表
                //转数字 入栈 清空临时多位数
                //numberStack.push(currentChar - 48);

                //以上代码不兼容多位数的情况，如下代码逻辑为兼容多位数的情况
                multiDigitNumber = multiDigitNumber + currentChar;


                //判断当前数字字符的下一个字符是否为操作符，如果是操作符那么就将临时字符串转数组入数字栈，否则将当前字符追加至临时多位数字符串的尾部。

                //如果已经是最后一个字符直接将临时多位数字符串转数字入栈
                if (expression.length() == index + 1) {
                    numberStack.push(Integer.parseInt(multiDigitNumber));
                } else {
                    int nextChar = expression.substring(index + 1, index + 2).charAt(0);
                    if (isOperator(nextChar)) {
                        //如果当前字符的下一位是操作符，那么就将保存临时多位数的字符串转数字并入栈，然后清空临时多位数字符串
                        numberStack.push(Integer.parseInt(multiDigitNumber));
                        //临时字符串用完了，清空！
                        multiDigitNumber = "";
                    }
                }
            }

            //处理下一个字符
            index++;

            if (expression.length() == index) {
                break;
            }

        }

        //循环处理完成，运算符已按照优先级放在操作符栈，按照计算规则从数字栈和运算符栈中弹出元素并计算
        while (!operatorStack.isEmpty()) {
            number2 = numberStack.pop();
            number1 = numberStack.pop();
            operator = operatorStack.pop();
            result = calculate(number1, number2, operator);
            numberStack.push(result);
        }

        System.out.println("计算结果为：" + numberStack.pop());

    }



    /**
     * 判断操作符的优先级
     * 逻辑：乘除的优先级高于加减
     * @return 1 表示是大优先级  0 表示是低优先级 非操作符返回-1
     *
     * */
    public static int checkPriority(int inputChar) {
        if (inputChar == '/' || inputChar == '*') {
            return 1;
        } else if (inputChar == '+' || inputChar == '-') {
            return 0;
        } else {
            return -1;
        }
    }

    /**
     * 判断输入的char是否是操作符
     * @return 如果是+-/*其中一个则返回true 否则返回 false
     * */
    public static boolean isOperator(int inputChar) {
        return inputChar == '+' || inputChar == '-' || inputChar == '*' || inputChar == '/';
    }


    /**
     * 根据输入值及操作符进行计算
     * @param number1 number1
     * @param number2 number2
     * @param operator 操作符
     * @return 结算结果
     *
     * */
    public static int calculate(int number1, int number2, int operator) {
        int result = 0;
        switch (operator) {
            case '+' -> {
                result = number1 + number2;
            }
            case '-' -> {
                result = number1 - number2;
            }
            case '*' -> {
                result = number1 * number2;
            }
            case '/' -> {
                result = number1 / number2;
            }
        }
        return result;
    }
}
