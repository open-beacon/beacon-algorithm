package com.beacon.stack;

/**
 * 栈的特点
 * 1、first in last out
 * 2
 * */
public class StackDemo {
    public static void main(String[] args) {
        ArrayStack arrayStack = new ArrayStack(10);

        arrayStack.push(1);
        arrayStack.push(2);
        arrayStack.push(3);
        arrayStack.push(4);
        arrayStack.push(5);
        arrayStack.push(6);

        arrayStack.showAllElement();

        arrayStack.pop();
        arrayStack.pop();
        arrayStack.pop();

        arrayStack.showAllElement();

        arrayStack.peak();
        arrayStack.showAllElement();

    }



    /**
     * 通过数组实现栈的特性
     * 1、定义一个变量top指向栈顶
     * 2、定义一个数组用户保存栈中的元素
     * 3、定义一个遍历maxSize表示栈的大小
     * */
    static class ArrayStack{
        private int top = -1;
        private final int[] elementArray;
        private final int maxSize;

        public ArrayStack(int maxSize) {
            this.maxSize = maxSize;
            this.elementArray = new int[maxSize];
        }

        /**
         * 判断是否栈满
         * */
        public boolean isFull() {
            return top == maxSize - 1;
        }

        /**
         * 判断是否栈空
         * */
        public boolean isEmpty() {
            return top == -1;
        }

        public void push(int element) {
            //判断是否栈满
            if (this.isFull()) {
                System.out.println("栈已经满了哦！");
                return;
            }
            elementArray[++top] = element;
        }

        public int pop() {
            if (this.isEmpty()) {
                throw new RuntimeException("栈是空的哦");
            }
            return elementArray[top--];
        }

        public int peak() {
            if (this.isEmpty()) {
                System.out.println("栈是空的");
                return 0;
            }
            System.out.println("当前栈的栈顶中的元素为：" + elementArray[top]);
            return elementArray[top];
        }

        /**
         * 展示所有元素
         */
        public void showAllElement() {
            if (this.isEmpty()) {
                System.out.println("当前栈中没有元素");
                return;
            }
            System.out.println("栈中的元素从顶部到底部的顺序如下：");
            for (int i = top; i >= 0; i--) {
                System.out.println(elementArray[i]);
            }
        }
    }
}
